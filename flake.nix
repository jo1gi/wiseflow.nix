{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }: 
  let
    pkgs = import nixpkgs { system = "x86_64-linux"; };
  in
  {
    packages.x86_64-linux = {

      wiseflow-device-monitor = pkgs.stdenv.mkDerivation rec {
        pname = "wiseflow-device-monitor";
        version = "2.3.0";

        src = builtins.fetchurl {
          url = "https://uniwise-wisemonitor.s3.eu-west-1.amazonaws.com/${version}/wiseflow_device_monitor_${version}_linux.deb";
          sha256 = "1nd3h4harbqk9cmhc757vy5sffiw6v9cvhbdmhclnjwsdgzp119l";
        };

        nativeBuildInputs = with pkgs; [
          autoPatchelfHook
          dpkg
        ];

        buildInputs = with pkgs; [
          webkitgtk
          xorg.xwininfo
        ];

        unpackPhase = "true";

        installPhase = ''
            mkdir -p $out/tmp
            dpkg -x $src $out/tmp
            cp -avr $out/tmp/usr/* $out
            rm -rf $out/tmp
            sed -i 's/Terminal=false/Terminal=true/g' $out/share/applications/wiseflow-device-monitor.desktop
        '';
      };

      fake-gnome-screenshot = pkgs.stdenv.mkDerivation rec {
        pname = "fake-gnome-screenshot";
        version = "1.0.0";

        src = ./scripts/gnome-screenshot.sh;

        unpackPhase = "true";

        buildInputs = [
          pkgs.bash
        ];

        installPhase = ''
          mkdir -p $out/bin
          cp $src $out/bin/gnome-screenshot
        '';

      };

      fake-xwininfo = pkgs.stdenv.mkDerivation rec {
        pname = "fake-xwininfo";
        version = "1.0.0";

        src = ./scripts/xwininfo.sh;

        unpackPhase = "true";

        buildInputs = [
          pkgs.bash
        ];

        installPhase = ''
          mkdir -p $out/bin
          cp $src $out/bin/xwininfo
        '';

      };

      wiseflow-proxy = pkgs.stdenv.mkDerivation rec {
        pname = "wiseflow-proxy";
        version = "1.0.0";

        src = ./scripts/wiseflow-proxy;

        unpackPhase = "true";

        buildInputs = [
          pkgs.mitmproxy
          pkgs.bash
        ];

        installPhase = ''
          mkdir -p $out/bin
          cp $src/wiseflow-proxy.sh $out/bin/wiseflow-proxy
          mkdir -p $out/share/wiseflow-proxy
          cp $src/wiseflow-proxy.py $out/share/wiseflow-proxy/wiseflow-proxy.py
        '';

      };

      default = self.packages.x86_64-linux.wiseflow-device-monitor;
    };

    devShells.x86_64-linux.default = pkgs.mkShell {
      buildInputs = [
        self.packages.x86_64-linux.wiseflow-device-monitor
        self.packages.x86_64-linux.fake-gnome-screenshot
        self.packages.x86_64-linux.fake-xwininfo
        self.packages.x86_64-linux.wiseflow-proxy
        pkgs.mitmproxy
      ];
      shellHook = ''
        export SSL_CERT_FILE="$HOME/.mitmproxy/mitmproxy-ca.pem";
      '';
      https_proxy="http://127.0.0.1:9976/";
      SSL_CERT_FILE="$HOME/.mitmproxy/mitmproxy-ca.pem";
    };

  };
}
