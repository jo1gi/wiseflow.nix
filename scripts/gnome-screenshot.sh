#!/usr/bin/env bash

last_arg="${@: -1}"
convert -size 100x100 canvas:white "PNG:$last_arg"
echo "$@" >> screenshot.log
